struct lista{
	int info;
	struct lista *prox;
};

typedef struct lista Lista;

struct lista2{
	int info;
	struct lista2 *ant;
	struct lista2 *prox;
};

typedef struct lista2 Lista2;

Lista* listaCria();
Lista* listaInsere(Lista *l, int i);
void listaImprimeRec(Lista *l);
void listaImprime(Lista *l);
int listaVazia(Lista *l);
Lista* listaBusca(Lista *l, int v);
Lista *listaRetira(Lista *l, int v);
void listaLibera(Lista *l);
void listaLiberaRec(Lista *l);
Lista *listaIntercala(Lista *l1,Lista *l2);
Lista *listaInsereOrdenado(Lista *l, int v);
int listaIgual(Lista *l1, Lista *l2);
void lcircImprime(Lista *l);
void listaCircularLibera(Lista *l);
Lista2 *lista2Insere(Lista2 *l, int v);
Lista2 *lista2Busca(Lista2 *l, int v);
Lista2 *lista2Retira(Lista2 *l, int v);



