#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "listas.h"

Lista* listaCria(){

	return NULL;
}

Lista* listaInsere(Lista *l, int i){

	Lista *novo = (Lista *)malloc(sizeof(Lista));

	novo->info = i;
	novo->prox = l;

	return novo;
}

void listaImprimeRec(Lista *l){

	if(l == NULL){
		return;
	}
	else{
		printf("Info %d \n",l->info);
		listaImprimeRec(l->prox);
	}


}


void listaImprime(Lista *l){

	Lista *p;

	for(p = l;p != NULL;p = p->prox){
		printf("Info %d \n", p->info);
		//printf("prox %p \n", p->prox);
	}
}

int listaVazia(Lista *l){

	return (l == NULL);
}

Lista* listaBusca(Lista *l, int v){

	Lista *p;

	for(p = l; p != NULL; p = p->prox){
		if(p->info == v){
			return p;
		}
	}

	return NULL;
}

Lista *listaRetira(Lista *l, int v){

	Lista *ant = NULL;
	Lista *p = l;

	while(p != NULL && p->info != v){

		ant = p;
		p = p->prox;
	}

	if(p == NULL)
		return l;

	if(ant == NULL)
		l = p->prox;
	else
		ant->prox = p->prox;

	free(p);

	return l;
}

Lista *listaRetiraRec(Lista *l, int v){

	if(l == NULL){
		return 0;
	}
	else{
		if(l->info==v){
			l=l->prox;
		}
		else{
				l->prox = listaRetiraRec(l->prox,v);
		}
	}
	return l;
}

void listaLibera(Lista *l){

	Lista *p = l;
	while(p != NULL){

		Lista *t = p->prox;
		free(p);
		p = t;
	}
}

void listaLiberaRec(Lista *l){

	if(l == NULL){
		return;
	}
	else{

		listaLiberaRec(l->prox);
		free(l);
	}

}

Lista *listaInsereOrdenado(Lista *l, int v){

	Lista *novo;
	Lista *ant = NULL;
	Lista *p = l;

	while(p != NULL && p->info < v){
		ant = p;
		p = p->prox;
	}

	novo = (Lista*)malloc(sizeof(Lista));
	novo->info = v;

	if(ant == NULL){
		novo->prox = l;
		l = novo;
	}
	else{
		novo->prox = ant->prox;
		ant->prox = novo;
	}
	return l;
}

int listaIgual(Lista *l1, Lista *l2){

	Lista *p = l1, *p1=l2;


	while(1){

		if(p->info!=p1->info){

			return 0;
		}
		if(p1->prox == NULL && p->prox==NULL){
			break;
		}
		p = p->prox;
		p1 = p1->prox;
	}

	return 1;
}

Lista *listaIntercala(Lista *l1,Lista *l2){

	Lista *p1 = l1,*p2 = l2,*d1,*d2;
	int i=0;

	while(p1 != NULL || p2 != NULL){


		if(p1->prox == NULL && p2->prox != NULL){
			if(i==0){
			p1->prox=l2;
			break;
			}
			else{
				p1->prox=d2;
				break;
			}
		}
		if(p1->prox != NULL && p2->prox == NULL){

			if(i==0){
				d1=p1->prox;
				p1->prox=p2;
				p2->prox=d1;
				break;
			}
			else{
				d1=p1->prox;
				p1->prox=d2;
				p1->prox->prox=d1;
				break;
			}
		}
		d1=p1->prox;
		d2=p2->prox;
		p1->prox=p2;
		p1->prox->prox=d1;
		p2=d2;
		p1=d1;
		i++;

	}



	return l1;
}



/*listas circulares*/
void lcircImprime(Lista *l){

	Lista *p = l;
	if(p)
		do{
			printf("%d\n", p->info);
			p=p->prox;
		}while(p!=l);
}

void listaCircularLibera(Lista *l){

	Lista *p = l;
	if(p)
		do{
			Lista *t = p->prox;
			free(p);
			p = t;
			p=p->prox;
		}while(p!=l);

}

/*listas duplamente encadeadas*/
Lista2 *lista2InsereDupla(Lista2 *l, int v){

	Lista2 *novo = (Lista2 *)malloc(sizeof(Lista2));
	Lista2 *daw;
	novo->info = v;
	novo->prox = l;
	daw=l->ant;
	daw->prox=novo;
	novo->ant = daw;
	if(l != NULL){
		l->ant = novo;
	}

	return novo;
}

Lista2 *lista2Insere(Lista2 *l, int v){

	Lista2 *novo = (Lista2 *)malloc(sizeof(Lista2));
	novo->info = v;
	novo->prox = l;
	novo->ant = NULL;
	if(l != NULL){
		l->ant = novo;
	}

	return novo;
}




Lista2 *lista2Busca(Lista2 *l, int v){

	Lista2 *p;

	for(p = l;p!= NULL;p=p->prox){
			if(p->info == v){
				return p;
			}
	}

	return NULL;
}

Lista2 *lista2Retira(Lista2 *l, int v){

	Lista2 *p = lista2Busca(l,v);
	if(p == NULL){
		return l;
	}

	if(l==p)
		l = p->prox;
	else
		p->ant->prox = p->prox;

	if(p->prox != NULL)
		p->prox->ant = p->ant;

	free(p);

	return l;
}